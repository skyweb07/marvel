import Foundation

extension NSObject {
    
    static var className: String {
        return String(describing: self)
    }
    
    static var nibName: String {
        return className.components(separatedBy: ".").last!
    }
}
