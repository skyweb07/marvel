import Foundation

extension Assembly {
    
    func getCharacterDataSource() -> CharacterDataSource {
        return CharacterDataSourceImpl(
            getAllCharactersFromApiDataSourceTask: getAllCharacterFromApiDataSource()
        )
    }
    
    func getComicDataSource() -> ComicDataSource {
        return ComicDataSourceImpl(
            getAllComicsFromApiDataSourceTask: getAllComicsFromApiDataSource()
        )
    }
}
