import Foundation

extension Assembly {
    
    func getHttpClient() -> HTTPClient {
 
        guard let httpClient = Resolve(URLSessionHTTPClient.self) else {
            
            let httpClient = URLSessionHTTPClient(
                urlSession: getUrlSession(),
                httpConfiguration: getHttpConfiguration(),
                httpRequestBuilder: getRequestBuilder()
            )
            
            httpClient.requestProcessors = [
                getMarvelApiClientRequestProcessor()
            ]
            
            return httpClient
        }
        
        return httpClient
    }
    
    private func getHttpConfiguration() -> HTTPConfiguration {
        let baseUrl = URL(string: "https://gateway.marvel.com/v1/public/")!
        
        return HTTPConfiguration(
            baseUrl: baseUrl
        )
    }
    
    private func getRequestBuilder() -> HTTPRequestBuilder {
        return URLSessionRequestBuilder()
    }
    
    private func getMarvelApiClientRequestProcessor() -> MarvelApiClientRequestProcessor {
        return MarvelApiClientRequestProcessor()
    }
}
