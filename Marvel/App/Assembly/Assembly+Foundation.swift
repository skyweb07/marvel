import UIKit

extension Assembly {
    
    func getApplication() -> UIApplication {
        return UIApplication.shared
    }
    
    func getApplicationWindow() -> UIWindow? {
        
        let frame = getMainScreen().bounds
        
        return UIWindow(
            frame: frame
        )
    }
    
    func getMainScreen() -> UIScreen {
        return UIScreen.main
    }
    
    func getUrlSession() -> URLSession {
        return URLSession.shared
    }
}
