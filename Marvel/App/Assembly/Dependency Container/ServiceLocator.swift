import Foundation

private final class ServiceLocator: DependencyContainer {
    
    fileprivate static let container = ServiceLocator()
    private var serviceInstances = [String: AnyObject]()
    
    private init() {}
    
    func register(key: String, instance: AnyObject) {
        serviceInstances[key] = instance
    }
    
    func get(key: String) -> AnyObject? {
        return serviceInstances[key]
    }
    
    func remove(key: String) {
        serviceInstances.removeValue(forKey: key)
    }
    
    func removeAll() {
        serviceInstances.removeAll()
    }
}

/**
 Returns the default dependency container
 */
func Container() -> DependencyContainer {
    return ServiceLocator.container
}

/**
 Register and save instance of an object
 
 - parameter service: service instance to register/locate
 - parameter key:     an key identifier
 
 - returns: returns a cached instance of the service
 */
func Register<T>(service: T, key: String) -> T where T: AnyObject {
    
    if let service = Container().get(key: key) {
        assertionFailure("Dependency is already registered \(service) -> \(key)")
    }
    
    ServiceLocator.container.register(key: key, instance: service)
    
    return service
}

/**
 Try to resolve an instance from the container
 
 - parameter key: instance key identifier
 
 - returns: wether the instance is registered
 */
func Resolve<T>(key: String) -> T? where T: AnyObject {
    return Container().get(key: key) as? T
}

func Resolve<T>(_: T.Type) -> T? where T: AnyObject {
    return Container().get(key: String(describing: T.self)) as? T
}
