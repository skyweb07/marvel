import Foundation

extension Assembly {
    
    func getCharacterDomainToViewMapper() -> CharacterDomainToViewMapper {
        return CharacterDomainToViewMapper()
    }
    
    func getComicDomainToViewMapper() -> ComicDomainToViewMapper {
        return ComicDomainToViewMapper()
    }
}
