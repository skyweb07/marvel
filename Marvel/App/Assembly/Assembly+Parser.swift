import Foundation

extension Assembly {
    
    func getCharacterApiParser() -> CharacterApiParser {
        return CharacterApiParser()
    }
    
    func getComicApiParser() -> ComicApiParser {
        return ComicApiParser()
    }
}
