import Foundation

extension Assembly {
    
    func getAllCharactersUseCase() -> GetAllCharactersUseCase {
        return GetAllCharactersUseCase(
            characterRepository: getCharacterRepository()
        )
    }
    
    func getAllComicsUseCase() -> GetAllComicsUseCase {
        return GetAllComicsUseCase(
            comicRepository: getComicRepository()
        )
    }
}
