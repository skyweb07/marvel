import Foundation

extension Assembly {
    
    func getCharacterRepository() -> CharacterRepository {
        return CharacterRepositoryImpl(
            characterDataSource: getCharacterDataSource()
        )
    }
    
    func getComicRepository() -> ComicRepository {
        return ComicRepositoryImpl(
            comicApiDataSource: getComicDataSource()
        )
    }
}
