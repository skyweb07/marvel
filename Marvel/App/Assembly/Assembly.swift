import Foundation

/**
 Default assembly to generate all the dependency tree, if you need a part of the framework
 just extend this struct with the appropiate naming
 
 `example: Assembly+ViewControllers.swift`
 */
final class Assembly {
    fileprivate static let defaultAssembly = Assembly()
    
    /**
     Prevent default initializer
     */
    private init() {}
}

/**
 Get dependency from assembly
 
 ```
 // Get comic repository instance
 
 Dependency().getComicRepository()
 ```
 
 - returns: the default assembly
 */
func Dependency() -> Assembly {
    return Assembly.defaultAssembly
}
