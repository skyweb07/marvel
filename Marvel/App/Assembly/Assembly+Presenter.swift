import Foundation

extension Assembly {
    
    func getCharacterListPresenter() -> CharacterListPresenter {
        return CharacterListPresenter(
            getAllCharactersUseCase: getAllCharactersUseCase(),
            characterDomainToViewMapper: getCharacterDomainToViewMapper()
        )
    }
    
    func getComicListPresenter() -> ComicListPresenter {
        return ComicListPresenter(
            getAllComicsUseCase: getAllComicsUseCase(),
            comicDomainToViewMapper: getComicDomainToViewMapper()
        )
    }
}
