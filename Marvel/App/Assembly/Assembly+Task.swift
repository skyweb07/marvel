import Foundation

extension Assembly {
    
    func getAllCharacterFromApiDataSource() -> GetAllCharactersFromApiDataSource {
        return GetAllCharactersFromApiDataSource(
            httpClient: getHttpClient(),
            characterApiParser: getCharacterApiParser()
        )
    }
    
    func getAllComicsFromApiDataSource() -> GetAllComicsFromApiDataSource {
        return GetAllComicsFromApiDataSource(
            httpClient: getHttpClient(),
            comicApiParser: getComicApiParser()
        )
    }
}
