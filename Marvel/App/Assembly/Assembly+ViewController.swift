import Foundation

extension Assembly: CharacterListViewControllerProvider {
    
    func characterListViewController() -> CharacterListViewController {
        let characterListViewController = CharacterListViewController(
            characterListPresenter: getCharacterListPresenter(),
            itemDetailViewControllerProvider: self
        )
        
        return characterListViewController
    }
}

extension Assembly: ComicListViewControllerProvider {
    
    func comicListViewController() -> ComicListViewController {
        let comicListViewController = ComicListViewController(
            comicListPresenter: getComicListPresenter(),
            itemDetailViewControllerProvider: self
        )
        
        return comicListViewController
    }
}

extension Assembly: ItemDetailViewControllerProvider {
    
    func itemDetailViewController(with: ItemDetailViewModel) -> ItemDetailViewController {
        return ItemDetailViewController(itemDetail: with)
    }
}
