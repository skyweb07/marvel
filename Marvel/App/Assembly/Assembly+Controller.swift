extension Assembly {
    
    func getApplicationController() -> ApplicationController {
        
        let applicationController = ApplicationController(
            windowCoordinator: getWindowCoordinator()
        )
        applicationController.window = getApplicationWindow()
        
        return applicationController
    }
    
    func getWindowCoordinator() -> WindowCoordinable {
        return DefaultWindowCoordinable(
            characterListViewControllerProvider: self,
            comicListViewControllerProvider: self
        )
    }
}
