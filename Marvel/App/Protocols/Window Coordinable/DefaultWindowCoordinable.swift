import UIKit

final class DefaultWindowCoordinable: NSObject, WindowCoordinable {
    
    private let characterListViewControllerProvider: CharacterListViewControllerProvider
    private let comicListViewControllerProvider: ComicListViewControllerProvider
    
    init(characterListViewControllerProvider: CharacterListViewControllerProvider,
         comicListViewControllerProvider: ComicListViewControllerProvider)
    {
        self.characterListViewControllerProvider = characterListViewControllerProvider
        self.comicListViewControllerProvider = comicListViewControllerProvider
    }
    
    func coordinate(_ window: UIWindow?) {
     
        let characterListViewController = characterListViewControllerProvider.characterListViewController()
        let characterListNavigationController = UINavigationController(
            rootViewController: characterListViewController
        )
   
        characterListNavigationController.tabBarItem.title = Translate("character_list.title")
        
        let comicListViewController = comicListViewControllerProvider.comicListViewController()
        let comicListNavigationController = UINavigationController(
            rootViewController: comicListViewController
        )
        
        comicListNavigationController.tabBarItem.title = Translate("comic_list.title")
        
        let tabBarController = UITabBarController()
        tabBarController.viewControllers = [
            characterListNavigationController,
            comicListNavigationController
        ]
    
        window?.rootViewController = tabBarController
        window?.makeKeyAndVisible()
    }
}
