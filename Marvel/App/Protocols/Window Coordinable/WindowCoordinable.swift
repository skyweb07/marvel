import UIKit

protocol WindowCoordinable {
    func coordinate(_ window: UIWindow?)
}
