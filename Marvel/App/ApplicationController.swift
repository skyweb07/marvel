import UIKit

 class ApplicationController: NSObject, UIApplicationDelegate {
    
    var window: UIWindow?
    
    private let windowCoordinator: WindowCoordinable
    
    init(windowCoordinator: WindowCoordinable) {
        self.windowCoordinator = windowCoordinator
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        windowCoordinator.coordinate(window)
        
        return true
    }
}
