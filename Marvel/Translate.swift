import Foundation

/**
 Returns a localized string from the main bundle
 
 - parameter key: localizable key identifier
 
 - returns: localized string
 */
func Translate(_ key: String) -> String {
    return NSLocalizedString(key, comment: "") // So Rebel, without comments 😎
}
