import PromiseKit

struct CharacterRepositoryImpl: CharacterRepository {

    private let characterDataSource: CharacterDataSource
    
    init(characterDataSource: CharacterDataSource) {
        self.characterDataSource = characterDataSource
    }
    
    func getAll() -> Promise<[Character]> {
        return characterDataSource.getAll()
    }
}
