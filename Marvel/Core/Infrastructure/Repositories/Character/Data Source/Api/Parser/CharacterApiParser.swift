import Foundation

private struct JSONKeys {
    static let identifier = "id"
    static let name = "name"
    static let description = "description"
 
    struct thumbNail {
        static let root = "thumbnail"
        static let path = "path"
        static let imageExtension = "extension"
    }
}

struct CharacterApiParser: Parseable {
    
    func parse(_ element: [String: AnyObject]) -> Character {
        
        let identifier = element[JSONKeys.identifier] as! Int
        let name = element[JSONKeys.name] as! String
        let description = element[JSONKeys.description] as? String
 
        let thumbNailPath = element[JSONKeys.thumbNail.root]?[JSONKeys.thumbNail.path] as? String
        let thumbNailExtension =  element[JSONKeys.thumbNail.root]?[JSONKeys.thumbNail.imageExtension] as? String
        
        var thumbNail: String?
        
        if let thumbNailPath = thumbNailPath, let thumbNailExtension = thumbNailExtension {
            thumbNail = "\(thumbNailPath).\(thumbNailExtension)"
        }
 
        return Character(
            identifier: identifier,
            name: name,
            description: description,
            thumbNail: thumbNail
        )
    }
}
