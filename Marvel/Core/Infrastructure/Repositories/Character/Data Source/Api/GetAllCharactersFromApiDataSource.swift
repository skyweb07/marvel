import PromiseKit

private struct JSONKeys {
    static let path = "characters"
    static let data = "data"
    static let results = "results"
}

struct GetAllCharactersFromApiDataSource: Task {
    
    private let httpClient: HTTPClient
    private let characterApiParser: CharacterApiParser
    
    init(httpClient: HTTPClient,
         characterApiParser: CharacterApiParser)
    {
        self.httpClient = httpClient
        self.characterApiParser = characterApiParser
    }
    
    func execute() -> Promise<[Character]> {
        
        let request = httpClient.GET(JSONKeys.path)
        
        return firstly {
        
            return httpClient.execute(request)
        
        }.then { httpResponse -> [Character] in
    
            guard let response = httpResponse.value as? [String: AnyObject] else {
                return []
            }

            return self.characterApiParser.parseArray(
                elements: response[JSONKeys.data]![JSONKeys.results] as! [[String: AnyObject]] // This is pottentially insecure but I'm doing this for the sake of the development process 😅
            )
        }
    }
}
