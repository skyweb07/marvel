import PromiseKit

struct CharacterDataSourceImpl: CharacterDataSource {
    
    private let getAllCharactersFromApiDataSourceTask: GetAllCharactersFromApiDataSource
    
    init(getAllCharactersFromApiDataSourceTask: GetAllCharactersFromApiDataSource) {
        self.getAllCharactersFromApiDataSourceTask = getAllCharactersFromApiDataSourceTask
    }
    
    func getAll() -> Promise<[Character]> {
        return getAllCharactersFromApiDataSourceTask.execute()
    }
}
