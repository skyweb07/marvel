import Foundation

private struct JSONKeys {
    static let title = "title"
    static let identifier = "id"
    static let digitalId = "digitalId"
    static let issueNumber = "issueNumber"
    static let description = "description"
    
    struct thumbNail {
        static let root = "thumbnail"
        static let path = "path"
        static let imageExtension = "extension"
    }
}

struct ComicApiParser: Parseable {
    
    func parse(_ element: [String: AnyObject]) -> Comic {
        
        let identifier = element[JSONKeys.identifier] as! Int
        let title = element[JSONKeys.title] as! String
        let digitalId = element[JSONKeys.digitalId] as! Int
        let issueNumber = element[JSONKeys.issueNumber] as! Int
        let description = element[JSONKeys.description] as? String
        
        let thumbNailPath = element[JSONKeys.thumbNail.root]?[JSONKeys.thumbNail.path] as? String
        let thumbNailExtension =  element[JSONKeys.thumbNail.root]?[JSONKeys.thumbNail.imageExtension] as? String
        
        var thumbNail: String?
        
        if let thumbNailPath = thumbNailPath, let thumbNailExtension = thumbNailExtension {
            thumbNail = "\(thumbNailPath).\(thumbNailExtension)"
        }

        return Comic(
            identifier: identifier,
            digitalId: digitalId,
            title: title,
            issueNumber: issueNumber,
            description: description,
            thumbNail: thumbNail
        )
    }
}
