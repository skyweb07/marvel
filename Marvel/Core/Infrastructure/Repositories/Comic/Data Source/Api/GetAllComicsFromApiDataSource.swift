import PromiseKit

private struct JSONKeys {
    static let path = "comics"
    static let data = "data"
    static let results = "results"
}

struct GetAllComicsFromApiDataSource: Task {
    
    private let httpClient: HTTPClient
    private let comicApiParser: ComicApiParser
    
    init(httpClient: HTTPClient,
         comicApiParser: ComicApiParser)
    {
        self.httpClient = httpClient
        self.comicApiParser = comicApiParser
    }
    
    func execute() -> Promise<[Comic]> {
        
        let request = httpClient.GET(JSONKeys.path)
        
        return firstly {
            
            return httpClient.execute(request)
            
        }.then { httpResponse -> [Comic] in
                
            guard let response = httpResponse.value as? [String: AnyObject] else {
                return []
            }
                
            return self.comicApiParser.parseArray(
                elements: response[JSONKeys.data]![JSONKeys.results] as! [[String: AnyObject]] // This is pottentially insecure but I'm doing this for the sake of the development process 😅
            )
        }
    }
}
