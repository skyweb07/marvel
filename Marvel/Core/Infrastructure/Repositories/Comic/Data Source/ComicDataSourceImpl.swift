import PromiseKit

struct ComicDataSourceImpl: ComicDataSource {
    
    private let getAllComicsFromApiDataSourceTask: GetAllComicsFromApiDataSource
    
    init(getAllComicsFromApiDataSourceTask: GetAllComicsFromApiDataSource) {
        self.getAllComicsFromApiDataSourceTask = getAllComicsFromApiDataSourceTask
    }
    
    func getAll() -> Promise<[Comic]> {
        return getAllComicsFromApiDataSourceTask.execute()
    }
}
