import PromiseKit

struct ComicRepositoryImpl: ComicRepository {
    
    private let comicApiDataSource: ComicDataSource
    
    init(comicApiDataSource: ComicDataSource) {
        self.comicApiDataSource = comicApiDataSource
    }
    
    func getAll() -> Promise<[Comic]> {
        return comicApiDataSource.getAll()
    }
}
