import Foundation

struct URLSessionRequestBuilder: HTTPRequestBuilder {
    
    func buildRequest(with method: HTTPRequestMethod, url: URL) -> HTTPRequest {
        return HTTPRequest(
            httpRequestMethod: method,
            url: url
        )
    }
    
    func buildUrlRequest(with method: HTTPRequestMethod, url: URL, headers: [[String : String]], parameters: [String: Any]) -> URLRequest {
        
        let components = NSURLComponents(url: url, resolvingAgainstBaseURL: false)
        
        parameters.forEach { key, value in
            components?.appendQueryItem(key: key, value: value as! String)
        }

        var request = URLRequest(url: components!.url!)
        
        for header in headers {
            for (key, value) in header {
                request.addValue(value, forHTTPHeaderField: key)
            }
        }

        request.httpMethod = method.rawValue

        return request
    }
}

