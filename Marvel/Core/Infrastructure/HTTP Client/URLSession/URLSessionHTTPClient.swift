import PromiseKit

enum URLSessionHTTPClientError: Error {
    case invalidResponse
}

final class URLSessionHTTPClient: HTTPClient {
    
    var requestProcessors = [HTTPRequestPreProcessor]()
    
    private let urlSession: URLSession
    private let httpConfiguration: HTTPConfiguration
    private let httpRequestBuilder: HTTPRequestBuilder
    
    init(urlSession: URLSession,
         httpConfiguration: HTTPConfiguration,
         httpRequestBuilder: HTTPRequestBuilder)
    {
        self.urlSession = urlSession
        self.httpConfiguration = httpConfiguration
        self.httpRequestBuilder = httpRequestBuilder
    }
    
    func GET(_ path: String) -> HTTPRequest {
        return httpRequestBuilder.buildRequest(with: .get, url: buildUrl(path))
    }
    
    func POST(_ path: String) -> HTTPRequest {
        return httpRequestBuilder.buildRequest(with: .post, url: buildUrl(path))
    }
    
    func PUT(_ path: String) -> HTTPRequest {
        return httpRequestBuilder.buildRequest(with: .put, url: buildUrl(path))
    }
    
    func PATCH(_ path: String) -> HTTPRequest {
        return httpRequestBuilder.buildRequest(with: .patch, url: buildUrl(path))
    }
    
    func DELETE(_ path: String) -> HTTPRequest {
        return httpRequestBuilder.buildRequest(with: .delete, url: buildUrl(path))
    }
    
    @discardableResult func execute(_ request: HTTPRequest) -> Promise<HTTPResponse> {
        
        return Promise { fulfill, reject in
            
            let processedRequest = preProcessRequest(
                request: request
            )
            
            let composedRequest = self.httpRequestBuilder.buildUrlRequest(
                with: processedRequest.getRequestMethod(),
                url: processedRequest.getUrl(),
                headers: processedRequest.getHeaders(),
                parameters: processedRequest.getParameters()
            )
 
            let task = self.urlSession.dataTask(with: composedRequest) { data, response, error in
                
                guard let data = data, let response = response else {
                    reject(error!)
                    return
                }
               
                let value = try? JSONSerialization.jsonObject(
                    with: data,
                    options: .allowFragments
                )

                let httpResponse = HTTPResponse(
                    data: data,
                    response: response,
                    value: value
                )

                if self.isNetworkError(response) {
                    
                    if let error = error {
                        reject(error)
                    } else {
                        reject(URLSessionHTTPClientError.invalidResponse)
                    }
                    
                    return
                }

                return fulfill(httpResponse)
            }
            
            task.resume()
        }
    }
 
    private func isNetworkError(_ response: URLResponse?) -> Bool {
        if let httpResponse = response as? HTTPURLResponse {
            return httpResponse.statusCode >= 400
        }
        return false
    }
    
    private func preProcessRequest(request: HTTPRequest) -> HTTPRequest {
        
        var requestToExecute = request
        
        for requestProcessor in requestProcessors {
            requestToExecute = requestProcessor.process(requestToExecute)
        }
        
        return requestToExecute
    }
    
    private func buildUrl(_ path: String) -> URL {
        return httpConfiguration.baseUrl.appendingPathComponent(path)
    }
}
