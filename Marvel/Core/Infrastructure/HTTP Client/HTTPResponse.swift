import Foundation

struct HTTPResponse {
    
    let data: Data
    let response: URLResponse
    let value: Any?
}
