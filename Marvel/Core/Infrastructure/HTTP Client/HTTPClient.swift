import PromiseKit

protocol HTTPClient {
 
    var requestProcessors: [HTTPRequestPreProcessor] { get set }
    
    func GET(_ path: String) -> HTTPRequest
    func POST(_ path: String) -> HTTPRequest
    func PUT(_ path: String) -> HTTPRequest
    func PATCH(_ path: String) -> HTTPRequest
    func DELETE(_ path: String) -> HTTPRequest
    
    func execute(_ request: HTTPRequest) -> Promise<HTTPResponse>
}
