import Foundation

enum HTTPRequestMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}

final class HTTPRequest {
    
    private var identifier: String
    private var httpRequestMethod: HTTPRequestMethod
    private var headers: [[String: String]]
    private var parameters: [String: Any]
    private var url: URL
 
    init(httpRequestMethod: HTTPRequestMethod, url: URL) {
        self.identifier = UUID().uuidString
        self.httpRequestMethod = httpRequestMethod
        self.headers = []
        self.parameters = [:]
        self.url = url
    }
    
    private init(identifier: String,
         httpRequestMethod: HTTPRequestMethod,
         headers: [[String: String]],
         parameters: [String: Any],
         url: URL)
    {
        self.identifier = identifier
        self.httpRequestMethod = httpRequestMethod
        self.headers = headers
        self.parameters = parameters
        self.url = url
    }
    
    func getRequestMethod() -> HTTPRequestMethod {
        return httpRequestMethod
    }
    
    func getHeaders() -> [[String: String]] {
        return headers
    }
    
    func getParameters() -> [String: Any] {
        return parameters
    }
    
    func getUrl() -> URL {
        return url
    }
    
    func addHeaders(_ headers: [String: String]) -> HTTPRequest {
        
        var newHeaders: [[String: String]] = [
            headers
        ]
        
        newHeaders.append(contentsOf: self.headers)
        
        return HTTPRequest(
            identifier: identifier,
            httpRequestMethod: httpRequestMethod,
            headers: newHeaders,
            parameters: parameters,
            url: url
        )
    }
    
    func addParameters(_ parameters: [String: Any]) -> HTTPRequest {
        
        let newParameters = self.parameters.union(parameters)
        
        return HTTPRequest(
            identifier: identifier,
            httpRequestMethod: httpRequestMethod,
            headers: headers,
            parameters: newParameters,
            url: url
        )
    }
}
