import Foundation

protocol HTTPRequestBuilder {
    
    func buildRequest(with method: HTTPRequestMethod, url: URL) -> HTTPRequest
    func buildUrlRequest(with method: HTTPRequestMethod, url: URL, headers: [[String : String]], parameters: [String: Any]) -> URLRequest
}
