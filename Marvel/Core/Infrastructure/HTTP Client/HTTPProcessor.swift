import Foundation

protocol HTTPRequestPreProcessor {
    
    func process(_ request: HTTPRequest) -> HTTPRequest
}
