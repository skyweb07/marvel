import Foundation

private struct MarvelAuthorizationKeys {
    static let publicKey = "8794659f5df079467363f87d98c67f5d"
    static let privateKey = "5775c118f6c805deada69177e6266612846a16de"
}

private struct Parameters {
    static let timestamp = "ts"
    static let apiKey = "apikey"
    static let hash = "hash"
}

/*
    This preprocessor will add to every request the required parameters
    to make an api call, all the process is documented on the following page
    http://developer.marvel.com/documentation/authorization ✅
 */

struct MarvelApiClientRequestProcessor: HTTPRequestPreProcessor {
    
    func process(_ request: HTTPRequest) -> HTTPRequest {
        
        let timestamp = String(describing: Date()).md5()
        let hash = "\(timestamp)\(MarvelAuthorizationKeys.privateKey)\(MarvelAuthorizationKeys.publicKey)".md5()
        
        return request.addParameters([
            Parameters.timestamp: timestamp,
            Parameters.apiKey: MarvelAuthorizationKeys.publicKey,
            Parameters.hash: hash
        ])
    }
}
