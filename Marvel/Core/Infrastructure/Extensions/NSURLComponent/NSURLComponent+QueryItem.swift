import Foundation

extension NSURLComponents {
    
    func appendQueryItem(key: String, value: String) {
        
        var queryItems = self.queryItems as [NSURLQueryItem]? ?? [NSURLQueryItem]()
        
        queryItems.append(
            NSURLQueryItem(name: key, value: value)
        )
        
        self.queryItems = queryItems as [URLQueryItem]?
    }
}
