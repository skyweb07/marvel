import Foundation

extension String {
    
    func md5() -> String {
        
        let digestLenght = Int(CC_MD5_DIGEST_LENGTH)
        var digest = [UInt8](repeating: 0, count: digestLenght)
        
        if let data = self.data(using: String.Encoding.utf8) {
            CC_MD5((data as NSData).bytes, CC_LONG(data.count), &digest)
        }
        
        var hash = ""
        
        for i in 0..<digestLenght {
            hash += String(format: "%02x", digest[i])
        }
        
        return hash
    }
}
