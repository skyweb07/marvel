import Foundation

extension Dictionary {
    
    func union(_ dictionary: Dictionary) -> Dictionary {
        return self.reduce(dictionary) { (d, p) in
            
            var derived = d
            
            derived[p.0] = p.1
            
            return derived
        }
    }
}
