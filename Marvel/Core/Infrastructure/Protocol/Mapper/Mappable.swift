import Foundation

protocol Mappable {
    
    associatedtype From
    associatedtype To
 
    func mapObject(_ from: From) -> To
}
