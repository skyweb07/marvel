import Foundation

extension Mappable {

    func mapObjects(objects: [From]) -> [To] {
        return objects.map { object -> To in
            return mapObject(object)
        }
    }
}
