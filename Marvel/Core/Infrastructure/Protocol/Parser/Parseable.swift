import Foundation

protocol Parseable {
    
    associatedtype Element
    associatedtype T

    func parse(_ element: Element) -> T
}

