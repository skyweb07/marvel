import Foundation

extension Parseable {
    
    func parseArray(elements: [Element]) -> [T] {
        return elements.map { element -> T in
            return parse(element)
        }
    }
}
