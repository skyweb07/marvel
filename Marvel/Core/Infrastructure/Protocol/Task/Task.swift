import PromiseKit

protocol Task {
    
    associatedtype T

    func execute() -> Promise<T>
}
