import PromiseKit

protocol CharacterDataSource {
    
    func getAll() -> Promise<[Character]>
}
