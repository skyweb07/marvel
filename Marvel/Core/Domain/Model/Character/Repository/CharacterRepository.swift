import PromiseKit

protocol CharacterRepository {
    
    func getAll() -> Promise<[Character]>
}
