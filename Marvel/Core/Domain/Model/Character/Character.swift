import Foundation

struct Character {
    
    let identifier: Int
    let name: String
    let description: String?
    let thumbNail: String?
}
