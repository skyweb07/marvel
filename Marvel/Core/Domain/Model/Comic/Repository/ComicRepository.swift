import PromiseKit

protocol ComicRepository {
    
    func getAll() -> Promise<[Comic]>
}
