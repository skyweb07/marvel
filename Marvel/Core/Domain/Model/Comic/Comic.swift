import Foundation

struct Comic {
    
    let identifier: Int
    let digitalId: Int
    let title: String
    let issueNumber: Int
    let description: String?
    let thumbNail: String?
}
