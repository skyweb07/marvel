import PromiseKit

protocol ComicDataSource {
    
    func getAll() -> Promise<[Comic]>
}
