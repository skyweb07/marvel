import PromiseKit

struct GetAllCharactersUseCase {
    
    private let characterRepository: CharacterRepository
    
    init(characterRepository: CharacterRepository) {
        self.characterRepository = characterRepository
    }
    
    func getAll() -> Promise<[Character]> {
        return characterRepository.getAll()
    }
}
