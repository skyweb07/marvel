import PromiseKit

struct GetAllComicsUseCase {
    
    private let comicRepository: ComicRepository
    
    init(comicRepository: ComicRepository) {
        self.comicRepository = comicRepository
    }
    
    func getAll() -> Promise<[Comic]> {
        return comicRepository.getAll()
    }
}
