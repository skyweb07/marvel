import Foundation

struct CharacterDomainToViewMapper: Mappable {
    
    func mapObject(_ from: Character) -> CharacterViewModel {
        
        var thumbNail: URL?
        
        if let url = from.thumbNail {
            thumbNail = URL(string: url)
        }
        
        return CharacterViewModel(
            identifier: from.identifier,
            name: from.name,
            description: from.description,
            thumbNail: thumbNail
        )
    }
}
