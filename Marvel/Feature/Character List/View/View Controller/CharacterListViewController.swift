import UIKit

protocol CharacterListViewControllerProvider {
    func characterListViewController() -> CharacterListViewController
}

private let cellIdentifier = "characterCell"

class CharacterListViewController: UIViewController, LoadableUI {

    fileprivate let characterListPresenter: CharacterListPresenter
    fileprivate let itemDetailViewControllerProvider: ItemDetailViewControllerProvider
    fileprivate var characters = [CharacterViewModel]()
    fileprivate var searchController: UISearchController?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    init(characterListPresenter: CharacterListPresenter,
         itemDetailViewControllerProvider: ItemDetailViewControllerProvider)
    {
        self.characterListPresenter = characterListPresenter
        self.itemDetailViewControllerProvider = itemDetailViewControllerProvider
        
        super.init(
            nibName: CharacterListViewController.className,
            bundle: nil
        )
        
        self.characterListPresenter.ui = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        let nib = UINib(nibName: CharacterCell.nibName, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: cellIdentifier)
        
        configureSearchController()
        
        characterListPresenter.viewDidLoad()
    }
    
    func beginLoading() {
        activityIndicator.startAnimating()
        activityIndicator.hidesWhenStopped = true
    }
    
    func endLoading() {
        activityIndicator.stopAnimating()
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError() }
}

// MARK: - CharacterListUI

extension CharacterListViewController: CharacterListUI {
    
    func setCharacters(_ characters: [CharacterViewModel]) {
        self.characters = characters
        tableView.reloadData()
    }
    
    func navigate(toDetail: ItemDetailViewModel) {
        
        let itemDetailViewController = itemDetailViewControllerProvider.itemDetailViewController(
            with: toDetail
        )
        
        navigationController?.pushViewController(itemDetailViewController, animated: true)
    }
}

// MARK: - UITableViewDataSource

extension CharacterListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        let character = characters[indexPath.row]

        cell.textLabel?.text = character.name
        cell.detailTextLabel?.text = character.description

        return cell
    }
}

// MARK: - UITableViewDelegate

extension CharacterListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let character = characters[indexPath.row]
        
        characterListPresenter.didSelectCharacter(character)
    }
}

// MARK: - UISearchResultsUpdating

extension CharacterListViewController: UISearchResultsUpdating, UISearchBarDelegate {
    
    func configureSearchController() {
 
        searchController = UISearchController(
            searchResultsController: nil
        )
        
        searchController?.searchResultsUpdater = self
        searchController?.dimsBackgroundDuringPresentation = false
        searchController?.hidesNavigationBarDuringPresentation = false
        searchController?.searchBar.searchBarStyle = .minimal
        searchController?.searchBar.delegate = self
        
        navigationItem.titleView = self.searchController?.searchBar

        definesPresentationContext = true
    }

    func updateSearchResults(for searchController: UISearchController) {
        characterListPresenter.filterCharacters(
            withQuery: searchController.searchBar.text
        )
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        characterListPresenter.didCancelSearch()
    }
}
