import PromiseKit

final class CharacterListPresenter {
    
    weak var ui: CharacterListUI?
    
    private let getAllCharactersUseCase: GetAllCharactersUseCase
    private let characterDomainToViewMapper: CharacterDomainToViewMapper
    
    private var characters = [CharacterViewModel]()
    
    init(getAllCharactersUseCase: GetAllCharactersUseCase,
         characterDomainToViewMapper: CharacterDomainToViewMapper)
    {
        self.getAllCharactersUseCase = getAllCharactersUseCase
        self.characterDomainToViewMapper = characterDomainToViewMapper
    }
    
    func viewDidLoad() {
        
        ui?.beginLoading()
        
        firstly {
            
            getAllCharactersUseCase.getAll()
            
        }.then { characters -> [CharacterViewModel] in
            
            return self.characterDomainToViewMapper.mapObjects(
                objects: characters
            )
            
        }.then { characterViewModels -> Void in
            
            self.characters = characterViewModels
            
            self.ui?.setCharacters(characterViewModels)
            
        }.always {
            
            self.ui?.endLoading()
            
        }.catch { error in
            print("Error getting characters => \(error)")
        }
    }
    
    func filterCharacters(withQuery: String?) {
        
        guard let query = withQuery else {
            ui?.setCharacters(characters)
            return
        }
        
        let filteredItems = characters.filter { character in
            character.name.lowercased().contains(query.lowercased())
        }
        
        ui?.setCharacters(filteredItems)
    }
    
    func didCancelSearch() {
        
        DispatchQueue.main.async {
            self.ui?.setCharacters(self.characters)
        }
    }
    
    func didSelectCharacter(_ character: CharacterViewModel) {
        
        let itemDetail = ItemDetailViewModel(
            title: character.name,
            description: character.description,
            thumbNail: character.thumbNail
        )
        
        ui?.navigate(
            toDetail: itemDetail
        )
    }
}
