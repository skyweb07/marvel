protocol CharacterListUI: class, LoadableUI {
    
    func setCharacters(_ characters: [CharacterViewModel])
    func navigate(toDetail: ItemDetailViewModel)
}
