import Foundation

struct CharacterViewModel {
    
    let identifier: Int
    let name: String
    let description: String?
    let thumbNail: URL?
}
