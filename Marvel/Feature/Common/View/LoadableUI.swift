import Foundation

protocol LoadableUI {
    
    func beginLoading()
    func endLoading()
}
