import Foundation

protocol ComicListUI: class, LoadableUI {
    
    func setComics(_ comics: [ComicViewModel])
    func navigate(toDetail: ItemDetailViewModel)
}
