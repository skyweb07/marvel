import UIKit
import SDWebImage

class ComicCell: UITableViewCell {

    @IBOutlet weak var comicThumbNail: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(_ comic: ComicViewModel) {
        
        titleLabel.text = comic.title
        descriptionLabel.text = comic.description ?? "No description"
        
        guard let thumnNailUrl = comic.thumbNail else {
            return
        }
        
        comicThumbNail.sd_setImage(with: thumnNailUrl)
    }
}
