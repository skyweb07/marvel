import UIKit

protocol ComicListViewControllerProvider {
    func comicListViewController() -> ComicListViewController
}

private let cellIdentifier = "comicCell"

class ComicListViewController: UIViewController, LoadableUI {
    
    fileprivate let comicListPresenter: ComicListPresenter
    fileprivate let itemDetailViewControllerProvider: ItemDetailViewControllerProvider
   
    fileprivate var comics = [ComicViewModel]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityLoading: UIActivityIndicatorView!
    
    init(comicListPresenter: ComicListPresenter,
         itemDetailViewControllerProvider: ItemDetailViewControllerProvider)
    {
        self.comicListPresenter = comicListPresenter
        self.itemDetailViewControllerProvider = itemDetailViewControllerProvider
        
        super.init(
            nibName: ComicListViewController.nibName,
            bundle: nil
        )
        
        self.comicListPresenter.ui = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: ComicCell.nibName, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: cellIdentifier)
        
        comicListPresenter.viewDidLoad()
    }
    
    func beginLoading() {
        activityLoading.startAnimating()
        activityLoading.hidesWhenStopped = true
    }
    
    func endLoading() {
        activityLoading.stopAnimating()
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError() }
}

// MARK: - UITableViewDataSource

extension ComicListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comics.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ComicCell
        let comic = comics[indexPath.row]
        
        cell.configureCell(comic)

        return cell
    }
}

// MARK: - UITableViewDelegate

extension ComicListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let comic = comics[indexPath.row]
        
        comicListPresenter.didSelectComic(comic)
    }
}

// MARK: - ComicListUI

extension ComicListViewController: ComicListUI {
    
    func setComics(_ comics: [ComicViewModel]) {
        self.comics = comics
        tableView.reloadData()
    }
    
    func navigate(toDetail: ItemDetailViewModel) {
        
        let itemDetailViewController = itemDetailViewControllerProvider.itemDetailViewController(
            with: toDetail
        )
        
        navigationController?.pushViewController(itemDetailViewController, animated: true)
    }
}
