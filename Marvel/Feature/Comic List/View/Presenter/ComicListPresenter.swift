import PromiseKit

final class ComicListPresenter {
    
    weak var ui: ComicListUI?
    
    private let getAllComicsUseCase: GetAllComicsUseCase
    private let comicDomainToViewMapper: ComicDomainToViewMapper

    private var comics = [ComicViewModel]()
    
    init(getAllComicsUseCase: GetAllComicsUseCase,
         comicDomainToViewMapper: ComicDomainToViewMapper)
    {
        self.getAllComicsUseCase = getAllComicsUseCase
        self.comicDomainToViewMapper = comicDomainToViewMapper
    }
    
    func viewDidLoad() {
        
        ui?.beginLoading()
        
        firstly {
            
            return getAllComicsUseCase.getAll()
            
        }.then { comics -> [ComicViewModel] in
        
            return self.comicDomainToViewMapper.mapObjects(
                objects: comics
            )
            
        }.then { comicViewModels -> Void in

            self.comics = comicViewModels
            
            self.ui?.setComics(comicViewModels)
        
        }.always {
        
            self.ui?.endLoading()
            
        }.catch { error in
            print("Error getting comic list \(error)")
        }
    }
    
    func didSelectComic(_ comic: ComicViewModel) {
        
        let itemDetail = ItemDetailViewModel(
            title: comic.title,
            description: comic.description,
            thumbNail: comic.thumbNail
        )
        
        ui?.navigate(
            toDetail: itemDetail
        )
    }
}
