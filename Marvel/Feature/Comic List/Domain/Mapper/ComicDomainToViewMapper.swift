import Foundation

struct ComicDomainToViewMapper: Mappable {
    
    func mapObject(_ from: Comic) -> ComicViewModel {
        
        var thumbNailUrl: URL?
        
        if let thumbNail = from.thumbNail {
            thumbNailUrl = URL(string: thumbNail)
        }
        
        return ComicViewModel(
            identifier: from.identifier,
            digitalId: from.digitalId,
            title: from.title,
            issueNumber: from.issueNumber,
            description: from.description,
            thumbNail: thumbNailUrl
        )
    }
}
