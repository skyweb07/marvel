import Foundation

struct ItemDetailViewModel {
    
    let title: String
    let description: String?
    let thumbNail: URL?
}
