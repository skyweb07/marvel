import UIKit
import SDWebImage

protocol ItemDetailViewControllerProvider {
    func itemDetailViewController(with: ItemDetailViewModel) -> ItemDetailViewController
}

class ItemDetailViewController: UIViewController {

    private let itemDetail: ItemDetailViewModel
    
    @IBOutlet weak var thumbNailImageView: UIImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    init(itemDetail: ItemDetailViewModel) {
        self.itemDetail = itemDetail
        
        super.init(
            nibName: ItemDetailViewController.nibName,
            bundle: nil
        )
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView(
            with: itemDetail
        )
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError() }
}

extension ItemDetailViewController {
    
    func configureView(with: ItemDetailViewModel) {
        title = with.title
        descriptionTextView.text = with.description ?? "No description"
        
        thumbNailImageView.sd_setImage(with: with.thumbNail)
    }
}
